/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'fidi.home', [ 
  'fidi.NotificationQueue',
  'ui.router'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'HomeCtrl', function HomeController( $scope, FidiNotificationQueue, FidiNotification, $q, $timeout) {

  var 
    deferred = $q.defer(),
    deferred2 = $q.defer()
  ;

  $scope.nm = [];
  $scope.nm2 = [];

  $timeout(function(){
    deferred.resolve();
  }, 2000);

  $timeout(function(){
    deferred2.resolve();
  }, 4000);
  
  var nq = new FidiNotificationQueue();

  nq.add(new FidiNotification({message: 'Test notification promise - 2000ms', promise: deferred.promise}));
  nq.add(new FidiNotification({message: 'Test notification autoclose  - default ms', autoclose: true}));
  nq.add(new FidiNotification({message: 'Test notification autoclose - 6000ms', autoclose: true, timeout: 6000}));

  var nq2 = new FidiNotificationQueue({containerId: 'wrap-notifications-second'});
  
  nq2.add(new FidiNotification({message: 'Test notification promise - 4000ms', promise: deferred2.promise}));
  nq2.add(new FidiNotification({message: 'Test notification autoclose  - default ms', autoclose: true}));
  nq2.add(new FidiNotification({message: 'Test notification autoclose - 7500ms', autoclose: true, timeout: 7500}));

  $scope.addM = function() {
    $scope.nm.push(nq.add(new FidiNotification({message: 'This notification will have to be manually removed | #' + ($scope.nm.length + 1), type: 'danger'})));
  };

  $scope.removeM = function() {
    nq.remove($scope.nm.shift());
  };

  $scope.addA1 = function() {
    nq.add(new FidiNotification({message: 'Test notification autoclose - 1500 ms', autoclose: true}));
  };
  $scope.addA3 = function() {
    nq.add(new FidiNotification({message: 'Test notification autoclose - 3000 ms', autoclose: true, timeout: 3000}));
  };
  $scope.addP = function() {
    var 
      deferred = $q.defer()
    ;

    $timeout(function(){
      deferred.resolve();
    }, 3000);

    nq.add(new FidiNotification({message: 'Test notification promise - 3000ms', promise: deferred.promise}));
  };

//-------------

  $scope.addSecondM = function() {
    $scope.nm2.push(nq2.add(new FidiNotification({message: 'This notification will have to be manually removed | #' + ($scope.nm2.length + 1), type: 'danger'})));
  };

  $scope.removeSecondM = function() {
    nq2.remove($scope.nm2.shift());
  };

  $scope.addSecondA1 = function() {
    nq2.add(new FidiNotification({message: 'Test notification autoclose - 1500 ms', autoclose: true}));
  };
  $scope.addSecondA3 = function() {
    nq2.add(new FidiNotification({message: 'Test notification autoclose - 3000 ms', autoclose: true, timeout: 3000}));
  };
  $scope.addSecondP = function() {
    var 
      deferred = $q.defer()
    ;

    $timeout(function(){
      deferred.resolve();
    }, 3000);

    nq2.add(new FidiNotification({message: 'Test notification promise - 3000ms', promise: deferred.promise}));
  };

})

;

