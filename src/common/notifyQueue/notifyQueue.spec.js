describe( 'NotificationQueue Service', function() {

  var 
    $document = null,
    $timeout = null,
    $animate = null,
    $rootScope = null,
    FidiNotificationQueue = null
  ;

  beforeEach( module( 'fidi.NotificationQueue' ) );

  beforeEach( inject(function(_$document_, _$timeout_, _$animate_, _$rootScope_, _FidiNotificationQueue_){
    $document = _$document_;
    $timeout = _$timeout_;
    $animate = _$animate_;
    $rootScope = _$rootScope_;
    FidiNotificationQueue = _FidiNotificationQueue_;

  }));

  afterEach( function(){
    $timeout.verifyNoPendingTasks();
  });

  it( 'should increment when new notification is added', inject( function() {
    var nq = new FidiNotificationQueue();

    nq.add({
      timeout: 5, 
      type: 'info'
    });

    expect(nq.status()).toBe(1);
  }));

  it( 'should ignore remove() when queue is empty', inject( function() {
    var nq = new FidiNotificationQueue();

    var n = nq.add({
      timeout: 5, 
      type: 'info', 
      destroy: function(){}
    });

    nq.remove(n);

    expect(nq.remove()).toBeFalsy();

  }));

  it( 'should ignore remove() if notification doesn\'t exist', inject( function() {
    var nq = new FidiNotificationQueue();

    var n = nq.add({
      timeout: 5, 
      type: 'info', 
      destroy: function(){}
    });

    expect(nq.remove({id: 2, destroy: function(){}})).toBe(-1);

  }));

  it( 'should decrement if notification is removed', inject( function() {
    var nq = new FidiNotificationQueue();

    var n = nq.add({
      timeout: 5, 
      type: 'info', 
      destroy: function(){}
    });

    expect(nq.status()).toBe(1);

    nq.remove(n);

    expect(nq.status()).toBe(0);
  }));

  it( 'should find a DOM element when a new NotificationQueue is created', inject( function($compile, $rootScope) {

    var html = '<div id="wrap-notifications"></div>';
    angular.element(document.body).append(html);

    var nq = new FidiNotificationQueue();
    var domElement = document.getElementById(nq.containerId);
    expect(domElement).toBeTruthy();

    html = '<div id="wrap-notifications-test"></div>';
    angular.element(document.body).append(html);
    nq = new FidiNotificationQueue({containerId: 'wrap-notifications-test'});
    domElement = document.getElementById(nq.containerId);
    expect(domElement).toBeTruthy();

    angular.element(domElement).remove();
    
  }));

  it( 'should add/remove DOM element when notification is added', inject( function($compile, $rootScope) {
    scope = $rootScope.$new();

    var html = '<div id="wrap-notifications"></div>';
    angular.element(document.body).append(html);

    var nq = new FidiNotificationQueue();
    nq.add({
      timeout: 3, 
      type: 'info',
      message: 'Test message 1', 
      destroy: function(){}
    });
    var lastN = nq.add({
      timeout: 3, 
      type: 'error', 
      message: 'Test message 2',
      destroy: function(){}
    });

    var containerElement = document.getElementById(nq.containerId);
    var itemsElements = angular.element(containerElement).find(nq.itemTag);
    expect(itemsElements.length).toBe(2);

    nq.remove(lastN);
    itemsElements = angular.element(containerElement).find(nq.itemTag);
    expect(itemsElements.length).toBe(1);

    angular.element(containerElement).remove();

  }));

  it( 'should use the promise to remove the element when it completes', inject( function($q) {

    var html = '<div id="wrap-notifications2"></div>';
    angular.element(document.body).append(html);

    var 
      notification,
      deferred = $q.defer()
    ;

    var nq = new FidiNotificationQueue({containerId: 'wrap-notifications2'});
    nq.add({
      timeout: 3, 
      type: 'error', 
      message: 'Test message promise 1',
      promise: deferred.promise,
      destroy: function(){}
    });
    
    var containerElement = document.getElementById(nq.containerId);
    var itemsElements = angular.element(containerElement).find(nq.itemTag);
    expect(itemsElements.length).toBe(1);

    deferred.resolve();
    $timeout.flush();
    $rootScope.$digest();

    itemsElements = angular.element(containerElement).find(nq.itemTag);
    expect(itemsElements.length).toBe(0);

    angular.element(containerElement).remove();

  }));

  it( 'should remove the item DOM when timeout expires', inject( function($q) {

    var html = '<div id="wrap-notifications3"></div>';
    angular.element(document.body).append(html);

    var 
      notification,
      deferred = $q.defer()
    ;

    var nq = new FidiNotificationQueue({containerId: 'wrap-notifications3'});
    nq.add({
      timeout: 3000, 
      type: 'error', 
      message: 'Test message promise 1',
      autoclose: true,
      destroy: function(){}
    });
    
    var containerElement = document.getElementById(nq.containerId);
    var itemsElements = angular.element(containerElement).find(nq.itemTag);
    expect(itemsElements.length).toBe(1);
    
    $timeout.flush();

    itemsElements = angular.element(containerElement).find(nq.itemTag);
    expect(itemsElements.length).toBe(0);

    angular.element(containerElement).remove();

  }));

//   it( 'should reuse the same DOM element if multiple is false and a new notification is added', inject( function() {
    
//   }));

//   it( 'should reuse the same DOM element if multiple is false and a new notification is removed', inject( function() {
    
//   }));


//   it( 'should dispatch an empty event when queue is empty', inject( function() {
    
//   }));

//   it( 'should dispatch a first event when a notification was added to an empty queue', inject( function() {
    
//   }));

//   it( 'should dispatch an add event when a new notification is added', inject( function() {
    
//   }));

//   it( 'should dispatch an add event when a new notification is added even when the queue is empty', inject( function() {
    
//   }));

});

