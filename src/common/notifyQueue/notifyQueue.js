angular.module( 'fidi.NotificationQueue', ['fidi.Notification'] )

.provider( 'FidiNotificationQueue', function() {

  var 
    $rootScope,
    $timeout
  ;

  //NotificationQueue object definition
  //-----------------------------------

  NotificationQueue.$const = {
    messages: {
      invalidType: 'Invalid type'
    },
    events: {
      complete: 'FidiNotificationQueue:COMPLETE'
    },
    itemIdPrefix: 'fidiN'
  };

  //default properties for a NotificationQueue
  var defaults = {
    queue: [],
    multiple: true,
    containerId: 'wrap-notifications',
    itemTag: 'p'
  };

  //the NotificationQueue constructor
  function NotificationQueue(params) {

    angular.extend(this, defaults);
    angular.extend(this, params); 

    this.container = angular.element(document.getElementById(this.containerId));

  }  

  NotificationQueue.lastId = 1;

  NotificationQueue.prototype.add = function(notification) {

    var self = this;
    
    notification.id = NotificationQueue.lastId++;
    this.queue.push(notification);

    //add DOM
    notification.element = this.container.append('<' + this.itemTag + ' id="' + NotificationQueue.$const.itemIdPrefix + notification.id + '" class="bg-' + notification.type + '">' + notification.message + '</' + this.itemTag + '>');
    notification.element = angular.element(document.getElementById(NotificationQueue.$const.itemIdPrefix + notification.id));

    //if has a promise then use it to remove the element when done
    if(notification.promise) {
      notification.promise['finally'](function(){
        self.remove(notification);
      });
    }

    //if autoclose then use it to remove the element when done
    if(notification.autoclose) {
      $timeout(function(){
        self.remove(notification); 
      }, notification.timeout);
    }

    return notification;

  };

  NotificationQueue.prototype.remove = function(notification) {

    var 
      self = this,
      found = false
    ;

    if(!self.queue.length || !notification) { return false; }
     
    angular.forEach(self.queue, function(n, index){
      if(n.id === notification.id) {
        removeDom(notification);
        notification.destroy();
        self.queue.splice(index, 1);
        found = true;
        return false;
      }
    });

    function removeDom(notification) { 
      if(notification.element) {  
        notification.element.remove();
      }
    } 

    if(found) {
      return notification;
    } else {
      return -1;
    }

  };

  NotificationQueue.prototype.status = function() {
    return this.queue.length;
  };

  //Provider return value
  //-----------------------------------
  this.$get = function fidiNotificationQueueFactory(_$rootScope_, _$timeout_) {

    $rootScope = _$rootScope_;
    $timeout = _$timeout_;

    return NotificationQueue;

  };

})

;


