angular.module( 'fidi.Notification', [] )

.provider( 'FidiNotification', function() {

  var 
    $rootScope,
    $timeout
  ;
  
  //Provider api
  //-----------------------------------

  //allows the user to define the notification types in the config phase
  this.setDefaults = function(defaults) {
    angular.extend(Notification.$defaults, defaults);
  };

  //Notification object definition
  //-----------------------------------

  Notification.$const = {
    messages: {
      invalidType: 'Invalid type'
    },
    events: {
      complete: 'FidiNotification:COMPLETE'
    }
  };

  //the Notification constructor
  function Notification(params) {

    var self = this;

    if(params && params.type && Notification.$defaults.types.indexOf(params.type) === -1) {
      throw new Error(Notification.$const.messages.invalidType);
    }
    
    if(params && params.promise) {
      params.promise.then(function(){
        if(self.destroyed) { return false; }
        $rootScope.$broadcast(Notification.$const.events.complete);
      });
    }
    
    if(params && params.autoclose && !params.promise) {
      this.$timeout = $timeout(function(){
        $rootScope.$broadcast(Notification.$const.events.complete);
        delete this['$timeout'];
      }, this.timeout);
    }
    angular.extend(this, params);
  }

  //Notification defaults
  Notification.$defaults = {
    types: ['info', 'warning', 'success', 'danger'],
    timeout: 3000
  };

  //default properties for a Notification
  var defaults = {
    id: -1,
    message: "",
    type: Notification.$defaults.types[0],
    timeout: Notification.$defaults.timeout,
    autoclose: false
  };

  //set the default Notification types (extend the prototype rather than the instance for performance reasons) 
  angular.extend(Notification.prototype, defaults);

  Notification.prototype.destroy = function() {
    if(this.$timeout) {
      $timeout.cancel(this.$timeout);
    }
    this.destroyed = true;
  };
  
  //Provider return value
  //-----------------------------------
  this.$get = function fidiNotificationFactory(_$rootScope_, _$timeout_) {

    $rootScope = _$rootScope_;
    $timeout = _$timeout_;

    return Notification;

  };

})

;

