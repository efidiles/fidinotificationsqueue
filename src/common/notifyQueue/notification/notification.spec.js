describe( 'FidiNotification Config', function() {

  var 
    newDefaults = {timeout: 5, types: ['testType']},
    FidiNotification
  ;

  beforeEach( module( 'fidi.Notification', function ( FidiNotificationProvider ) {
      FidiNotificationProvider.setDefaults(newDefaults);
  } ) );

  beforeEach( inject( function ( _FidiNotification_ ) {
      FidiNotification = _FidiNotification_;
  }));

  afterEach(function(){
    FidiNotification = null;
  });

  it( 'should allow setting new defaults in config phase', function() {

    expect( FidiNotification.$defaults.timeout ).toBe(5);
    expect( FidiNotification.$defaults.types[0] ).toBe('testType');

  });

});

describe( 'FidiNotification Factory', function() {

  var 
    FidiNotification
  ;

  beforeEach( module( 'fidi.Notification' ) );

  beforeEach( inject( function ( _FidiNotification_ ) {
      FidiNotification = _FidiNotification_;
  }));

  it( 'should have default values set', inject( function() {
    var notification = new FidiNotification();
    expect(notification.message).toBeDefined();
    expect(notification.type).toBeDefined();
    expect(notification.timeout).toBeDefined();
  }));

  it( 'should throw error if unknown type was provided', inject( function() {
    expect(function() { new FidiNotification({type: 'wrongType'}); } ).toThrow(new Error(FidiNotification.$const.messages.invalidType));
  }));

  it( 'should dispatch COMPLETE event when the promise completes', inject( function($rootScope, $q) {
    
    var 
      notification,
      deferred = $q.defer()
    ;

    spyOn($rootScope, "$broadcast");

    notification = new FidiNotification({promise: deferred.promise});
    deferred.resolve();
    $rootScope.$digest();

    expect($rootScope.$broadcast).toHaveBeenCalledWith(FidiNotification.$const.events.complete);

  }));

  it( 'should dispatch COMPLETE event when autoclose is true', inject( function($rootScope, $timeout) {
    
    var 
      notification
    ;

    spyOn($rootScope, "$broadcast");

    notification = new FidiNotification({autoclose: true});
    $timeout.flush();

    expect($rootScope.$broadcast).toHaveBeenCalledWith(FidiNotification.$const.events.complete);

  }));

  it( 'should not use the timeout if a promise is set', inject( function($rootScope, $q, $timeout) {
    
    var 
      notification,
      deferred = $q.defer()
    ;

    spyOn($rootScope, "$broadcast");

    notification = new FidiNotification({promise: deferred.promise, autoclose: true});
    deferred.resolve();
    //flush the promise timeout
    $timeout.flush(1);
    $rootScope.$digest();

    $timeout.verifyNoPendingTasks();
    expect($rootScope.$broadcast).toHaveBeenCalledWith(FidiNotification.$const.events.complete);

  }));

  it( 'should cancel the promise and timeout when destroy is called', inject( function($rootScope, $timeout, $q) {
    
    var 
      notification,
      deferred = $q.defer()
    ;

    spyOn($rootScope, "$broadcast");

    notification = new FidiNotification({autoclose: true});
    notification.destroy();

    notification = new FidiNotification({promise: deferred.promise});
    notification.destroy();
    deferred.resolve();
    $timeout.flush();
    $rootScope.$digest();

    $timeout.verifyNoPendingTasks();
    expect($rootScope.$broadcast).not.toHaveBeenCalled();

  }));

});

